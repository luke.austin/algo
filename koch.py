import math

def rotate(C,D):
    x = (C[0]+D[0])*math.cos(math.radians(60)) - (D[1]+C[1])*math.sin(math.radians(60))
    y = (C[1]+D[1])*math.cos(math.radians(60)) + (D[0]-C[0])*math.sin(math.radians(60))
    return (x,y)

def add(U,V):
    return (U[0]+V[0],U[1]+U[1])

def mult(U,scalaire):
    return(U[0]*scalaire,U[1]*scalaire)
    

def point_inter(A,B):
    C = (((A[0]+(B[0]-A[0]))/3),A[1]+(B[1]-)
    D = 
    E = rotate(C,D)
    return (C,D,E) 

def flocon(A,B,n):
    if(n==0):
        print(A[0]," ",A[1])
        print(B[0]," ",B[1])
    else:
        (C,D,E) = point_inter(A,B)
        flocon(A,C,n-1)
        flocon(C,E,n-1)
        flocon(E,D,n-1)


flocon((30.0,30.0),(60.0,30.0),2)
