
def racineCarreApprochee(A) :
    X = A
    Epsilon = 0.00001
    
    while (abs(X*X-A)>Epsilon) :
        X = 0.5*(X + (A/X))
    return X

# Table de multiplication
def tableMultiplication(n) :
    for i in range (1,11) :
        print i,"*",n,"\t",i*n
    print
    return

# suite de Fibonacci
# Version naive recursive
def fiboRec(n) :
    if (n>1) :
        res = fiboRec(n-1) + fiboRec(n-2)
    else :
        res = 1
    return res


# Version recursive terminale
def fiboRecTerm(n, prec, acc) :
    if (n==0) :
        res = acc
    else :
        res = fiboRecTerm(n-1,acc,acc+prec)
    return res


# Version iterative
def fiboIter(n) :
    res=1
    prec=0
    for i in range (0,n) :
        tmp = prec
        prec=res
        res=res+tmp
    return res


# calcul de la somme 1/(2^i), depuis i=0 jusque n
def sommeUnSurDeuxI(n) :
    s=0
    t=1.0
    for i in range (0,n+1) :
        s = s+t
        t = t/2.0
    return s


# La meilleure methode pour calculer cette somme s avere etre la suivante !
def sommeUnSurDeuxI_2(n) :
    return 2.0 * (1.0 - (0.5 ** float(n+1)))


x = racineCarreApprochee(2)
print "Raccine de 2 : ", x, " (",x*x,")"
x = racineCarreApprochee(3)
print "Raccine de 3 : ", x, " (",x*x,")"
x = racineCarreApprochee(5)
print "Raccine de 5 : ", x, " (",x*x,")"
print
print

y=3
tableMultiplication(y)
y=8
tableMultiplication(y)
print
print

y=0
print(fiboRec(y),fiboRecTerm(y,0,1),fiboIter(y))
y=1
print(fiboRec(y),fiboRecTerm(y,0,1),fiboIter(y))
y=5
print(fiboRec(y),fiboRecTerm(y,0,1),fiboIter(y))
y=10
print(fiboRec(y),fiboRecTerm(y,0,1),fiboIter(y))
print
print
    
y=0
z=sommeUnSurDeuxI(y)
print(z)
z=sommeUnSurDeuxI_2(y)
print(z)
y=1
z=sommeUnSurDeuxI(y)
print(z)
z=sommeUnSurDeuxI_2(y)
print(z)
y=2
z=sommeUnSurDeuxI(y)
print(z)
z=sommeUnSurDeuxI_2(y)
print(z)
      

