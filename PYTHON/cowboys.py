import random;

class Cowboy:
    def __init__(self,poids,vitesse):
        self.armed = False;
        self.poids = poids;
        self.vitesse = vitesse;

    def arm(self):
        self.armed = not self.armed;

    def toString(self):
        print("armed: ",self.armed," poids: ",self.poids," vitesse: ",self.vitesse);

class Pont:
    def __init__(self,longueur,cowboys = []):
        self.debut = cowboys;
        self.fin = [];
        self.longueur = longueur;

    def cross(self, cowboys = []):
        self.fin.append(cowboys);
        self.debut.remove(cowboys);

    def goBack(self, cowboys = []):
        self.debut.append(cowboys);
        self.fin.remove(cowboys);

    def toString(self):
        print("taille du pont: ",self.longueur);
        print("cowboys à gauche: ");
        for cowboys in self.debut:
            cowboys.toString();
        print("cowboys arrivés: ");
        for cowboys in self.fin:
            cowboys.toString();
       


nbr_c = random.randint(1,6);

print('nmbre de cowboys: ',nbr_c);

cowboys = [];

for i in range(0,nbr_c):
    cowboys.append(Cowboy(random.randint(1,9),random.randint(1,9)));

cowboys[0].arm();
pont = Pont(random.randint(4,14),cowboys);

pont.toString();
