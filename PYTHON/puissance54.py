import math
# élever à la puissance 54. 

def p(x):
    three = x*x*x
    nine = three*three*three
    twentyseven = nine*nine*nine
    fiftyfour = twentyseven*twentyseven
    return fiftyfour
                                               
print('nombre: ',end=' ')
x = int(input())

x54 = p(x)

print('puissance : ',x54)
print('(check : ', x**54),')')

