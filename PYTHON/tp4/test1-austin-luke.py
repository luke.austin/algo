# austin luke

def monnaie(montant):
    fifty = twenty = ten =  five = two = one = 0;

    if(montant >= 50):
        fifty = 1;
        montant = montant - 50;

    while(montant >= 20):
        twenty += 1;
        montant = montant - 20;
 
    while(montant >= 10):
        ten += 1;
        montant = montant - 10;

    while(montant >= 5):
        five += 1;
        montant = montant - 5;   

    while(montant >= 2):
        two += 1;
        montant = montant - 2;

    while(montant >= 1):
        one += 1;
        montant = montant - 1;

    print("cinquantes: ",fifty," vingts: ",twenty," dix: ",ten," cinqs: ",five," deux: ",two," uns: ",one);
    return;

def diviseur(a, b):
    if(a%b == 0):
        print(b, " est un diviseur de ",a);
    else:
        print(b, "n\'est pas un diviseur de ",a);
    return;

def greatestK(r):
    past_k = 0;
    k = 0;
    while(2**k <= r):
        past_k = k;    
        k = k + 1;

    print("r: ",r," k: ",past_k," 2^k: ",2**past_k);
    return;

# -- Main -- 
print("exercice 1");
montant = int(input("Montant (0 - 99):"));

if(montant <= 99 and montant >= 0):
    monnaie(montant);
else:
    print("valeur invalide");

print("exercice 2");
a = int(input("a: "));
b = int(input("b: "));

diviseur(a,b);

print("exercice 3");
r = float(input("r (>1): "));
if(r > 1.0):
    greatestK(r);
else:
    print("valeur invalide");
