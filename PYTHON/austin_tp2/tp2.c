# austin luke

# calcul du racine carré approché
def rac(a): 
    xj = a
    i = 0

    while ((xj != x*x) and (i < 1000)):
        xj = (xj + (a/xj)) / 2
        i = i+1

    return xj

# tableau de multiplication
def mult(x):
    print('-----------------------')
    for i in range(1,11):    
        print('|    ',i,'*',x,' |   ',i*x,' |')
    print('-----------------------')
    return

# suite de fibonacci
def fib(n, a, b):
    print(b)
    if (n <= 1):
        return b
    else:
        val = fib(n-1,b,b+a)
        return val  

# somme de termes d'une suite
def somme(n):
    res = 0
    term = 0
    for i in range(1,n+1):
        term = 0.5**i
        res = res + term
        print('n(',i,') = ',res)
    return res

# main
print('----Racine carrée-----')
x = float(input())
print('Racine carrée de ',x,' = ',rac(x))


print('----Table de multiplication-----')
x = int(input())
mult(x)


print('----Suite de Fibonacci-----')
n = int(input())
fib(n,0,1)

print('-------Somme de termes d\'une suite--------')
x = int(input())
res = somme(x)
