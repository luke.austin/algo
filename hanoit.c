#include <stdio.h>

void hanoi(int n, char G, char D, char M)
{
	if(n > 0)
	{
		hanoi(n-1, G, M, D);
		printf("mouvement de %c vers %c",G,D);
		hanoi(n-1,M, D, G);
	}
}

int main(void)
{
	hanoi(12,'G','D','L');
	return 0;
}
