#include <stdio.h>

void monnaie (int s) {
    if (s>=50) {
        printf("1 pièce de 50 cents\n");
        s=s-50;
    }
    if (s>=40) {
        printf("2 pièces de 20 cents\n");
        s=s-40;
    }
    if (s>=20) {
        printf("1 pièce de 20 cents\n");
        s=s-20;
    }
    if (s>=10) {
        printf("1 pièce de 10 cents\n");
        s=s-10;
    }
    if (s>=5) {
        printf("1 pièce de 5 cents\n");
        s=s-5;
    }
    if (s>=4) {
        printf("2 pièces de 2 cents\n");
        s=s-4;
    }
    if (s>=2) {
        printf("1 pièce de 2 cents\n");
        s=s-2;
    }
    if (s==1) {
        printf("1 pièce de 1 cents\n");
    }
}

int diviseurViseur(int a, int b) {
    return ((a%b)==0);
}

int plusGrandDesPlusPetits(float r) {
    int res=0;
    int p2=1;
    while(p2<=r) {
        res=res+1;
        p2=2*p2;
    }
    return res-1;
}

    
int main() {
    monnaie(99);
    printf("%d\n",diviseurViseur(12,3));
    printf("%d\n",diviseurViseur(12,5));
    printf("%d\n",plusGrandDesPlusPetits(100));
    printf("%d\n",plusGrandDesPlusPetits(64));

    return 0;
}
