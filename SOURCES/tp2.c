#include <stdio.h>
#include <math.h>

/* compiler par gcc -o tp2 tp2.c -Wall -Wextra -lm */
/* calcul approché de la racine carrée */
double racineCarreApprochee(double A) {
    double X = A, Epsilon = 0.00001;
    
    while(fabs(X*X-A)>Epsilon) {
        X = 0.5*(X + (A/X));
    }
    return X;
}

/* Table de multiplication */
void tableMultiplication(int n) {
    int i;
    for(i=1;i<=10;i++) {
        printf("%2d*%d\t%2d\n",i,n,i*n);
    }
    printf("\n");
}

/* suite de Fibonacci */
/* Version naïve récursive */
int fiboRec(int n) {
    int res=1;
    if (n>1) res = fiboRec(n-1) + fiboRec(n-2);
    return res;
}

/* Version récursive terminale */
int fiboRecTerm(int n, int prec, int acc) {
    int res;
    if (n==0) res = acc;
    else res = fiboRecTerm(n-1,acc,acc+prec);
    return res;
}

/* Version itérative */
int fiboIter(int n) {
    int res=1, prec=0, i;
    for (i=0;i<n;i=i+1) {
        int tmp=prec;
        prec=res;
        res=res+tmp;
    }
    return res;
}

/* calcul de la somme 1/(2^i), depuis i=0 jusque n */
float sommeUnSurDeuxI(int n) {
    float s=0, t=1.0;
    int i;
    for(i=0; i<=n; i++) {
        s = s+t;
        t = t/2.0;
    }
    return s;
}

/* La meilleure méthode pour calculer cette somme s’avère être la suivante ! */
float sommeUnSurDeuxI_2(int n)
{
    return 2.0 * (1.0 - powf(0.5, (float)(n+1)));
}


int main() {
    
    double x;
    int y;
    float z;
    
    x = racineCarreApprochee(2);
    printf("Raccine de %lf : %lf (%lf)\n",2.0, x, x*x);
    x = racineCarreApprochee(3);
    printf("Raccine de %lf : %lf (%lf)\n",3.0, x, x*x);
    x = racineCarreApprochee(5);
    printf("Raccine de %lf : %lf (%lf)\n",5.0, x, x*x);
    printf("\n\n");
    
    y=3;
    tableMultiplication(y);
    y=8;
    tableMultiplication(y);
    printf("\n\n");
    y=0;
    printf("%d %d %d\n",fiboRec(y),fiboRecTerm(y,0,1),fiboIter(y));
    y=1;
    printf("%d %d %d\n",fiboRec(y),fiboRecTerm(y,0,1),fiboIter(y));
    y=5;
    printf("%d %d %d\n",fiboRec(y),fiboRecTerm(y,0,1),fiboIter(y));
    y=10;
    printf("%d %d %d\n",fiboRec(y),fiboRecTerm(y,0,1),fiboIter(y));
    printf("\n\n");
    
    y=0;
    z=sommeUnSurDeuxI(y);
    printf("%f ",z);
    z=sommeUnSurDeuxI_2(y);
    printf("%f\n",z);
    y=1;
    z=sommeUnSurDeuxI(y);
    printf("%f ",z);
    z=sommeUnSurDeuxI_2(y);
    printf("%f\n",z);
    y=2;
    z=sommeUnSurDeuxI(y);
    printf("%f ",z);
    z=sommeUnSurDeuxI_2(y);
    printf("%f\n",z);
    
    return 0;
}
