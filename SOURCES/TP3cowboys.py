import random

def initListeCowBoys(n) :
    l=[]
    for i in range(0,n) :
        p = random.randint(50,120) #kg
        v = random.randint(1,5) # m/s
        l.append((p,v))
    L = 50 # longueur du pont en m
    C = 300 # capacite du pont en kg
    return (l,L,C)

def getFastest(l) :
	return max(l, key = lambda t: t[1])

def getLightetst(l) :
    return min(l)

def moveFirsts(p, lcb, C) :
    res=[]
    loop=True
    s=p
    while loop :
        if len(lcb)==0 :
            loop = False
        elif s > C :
            loop = False
        else :
            (lp, lv) = lcb[0]
            if (s+lp)>C :
                loop = False
            else :
                res.append(lcb[0])
                del lcb[0]
                s = s + lp
    return res

def getSlowest(l) :
    return min(l, key = lambda t: t[1])



def strategyLightestsFirst(lcb, L, C) :
    (p,v) = getLightetst(lcb)
    lcb.remove((p,v))
    lcb.sort()

    t = 0
    moves=1
    transferred=[]

    loop=True
    while loop :
        if len(lcb)==0:
            loop = False
        else :
            group=moveFirsts(p, lcb, C)
            (sp, sv) = getSlowest(group+[(p,v)])
            transferred = transferred + group
            t = t + (float(L)/float(sv))
            if len(lcb)>0:
                t = t + (float(L)/float(v))
                moves = moves+1
    return (moves,t)

def strategyFastestsFirst(lcb, L, C) :
    (p,v) = getFastest(lcb)
    lcb.remove((p,v))
    l2=[(f1,f2) for f2,f1 in lcb]
    l2.sort()
    l2.reverse()
    lcb=[(f1,f2) for f2,f1 in l2]
    
    t = 0
    moves=1
    transferred=[]
    
    loop=True
    while loop :
        if len(lcb)==0:
            loop = False
        else :
            group=moveFirsts(p, lcb, C)
            (sp, sv) = getSlowest(group+[(p,v)])
            transferred = transferred + group
            t = t + (float(L)/float(sv))
            if len(lcb)>0:
                t = t + (float(L)/float(v))
                moves = moves+1
    return (moves,t)

def strategyNoStrategy(lcb, L, C) :
    (p,v) = lcb[0]
    lcb.remove((p,v))
    
    t = 0
    moves=1
    transferred=[]
    
    loop=True
    while loop :
        if len(lcb)==0:
            loop = False
        else :
            group=moveFirsts(p, lcb, C)
            (sp, sv) = getSlowest(group+[(p,v)])
            transferred = transferred + group + [(p,v)]
            t = t + (float(L)/float(sv))
            (p,v) = group[0]
            if len(lcb)>0:
                (p,v) = group[0]
                transferred.remove((p,v))
                t = t + (float(L)/float(v))
                moves = moves+1
    return (moves,t)

random.seed()
(lcb,L,C) = initListeCowBoys(30)
tmp=lcb[:]
(moves, duration) = strategyLightestsFirst(lcb, L, C)
print (moves, duration)
lcb=tmp[:]
(moves, duration) = strategyFastestsFirst(lcb, L, C)
print (moves, duration)
lcb=tmp[:]
(moves, duration) = strategyNoStrategy(lcb, L, C)
print (moves, duration)









