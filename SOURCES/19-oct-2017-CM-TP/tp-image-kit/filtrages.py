
from PIL import Image

def getMedian(pixels):
    pixels.sort();
    middel = len(pixels)//2;
    return pixels[middel];


def getMoyenne(pixels):
    somme = 0
    for px in pixels:
        somme = somme + px
        
    return somme//len(pixels);


def filtrerMedianImageNB(src,  sy,  sx, tailleFiltre) :
    res = Image.new("L",src.size,"black")
    neighbors = [];

    half = tailleFiltre//2;
    n = [];
    for i  in range (1,half):
        n.append(i);
        n.append(-i);

    
    for y in range (0,sy):
        for x in range (0,sx):
            neighbors.append(src.getpixel((x,y)))
            for k in n:
                if(y+k < sy and y+k >= 0):
                    neighbors.append(src.getpixel((x,y+k)))
                    if(x-k < sx and x-k >= 0): 
                        neighbors.append(src.getpixel((x-k,y+k)))
                if(x+k < sx and x+k >= 0):
                    neighbors.append(src.getpixel((x+k,y)))
                    if(y+k < sy and y+k >= 0): 
                        neighbors.append(src.getpixel((x+k,y+k)))
            res.putpixel((x,y),getMedian(neighbors));        
            neighbors = [];

    return res

def filtrerImageNB(src,  sy,  sx, tailleFiltre) :
    res = Image.new("L",src.size,"black")
    neighbors = [];
    half = tailleFiltre//2;
    n = [];
    for i  in range (1,half):
        n.append(i);
        n.append(-i);
    for y in range (0,sy):
        for x in range (0,sx):
            neighbors.append(src.getpixel((x,y)))
            for k in n:
                if(y+k < sy and y+k >= 0):
                    neighbors.append(src.getpixel((x,y+k)))
                    if(x-k < sx and x-k >= 0): 
                        neighbors.append(src.getpixel((x-k,y+k)))
                if(x+k < sx and x+k >= 0):
                    neighbors.append(src.getpixel((x+k,y)))
                    if(y+k < sy and y+k >= 0): 
                        neighbors.append(src.getpixel((x+k,y+k)))
            res.putpixel((x,y),getMoyenne(neighbors));        
            neighbors = [];

    return res

