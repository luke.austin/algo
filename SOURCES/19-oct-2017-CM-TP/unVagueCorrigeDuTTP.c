
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*!
 \file effets.c
 \author
 \date
 \brief module des fonctions de correction des images
 */

/*!
 \fn void inversion_video(unsigned char *img, int n)
 \brief transforme l'image en "négatif"
 \param img : adresse du bitmap
 \param n : nombre d'octets de l'image = largeur * hauteur * taille de pixel

 */
void inversion_video(unsigned char *img, int n)
{
    int i;
    for(i=0;i<n;i=i+1)
        img[i]=255-img[i];
}

/*!
 \fn void plusClair(unsigned char *img, int n, int nbplan)
 \brief rend l'image plus claire en augmentant les niveaux de 5%. Les niveaux sont tronqués à 255.
 \param img : adresse du bitmap
 \param n : nombre de pixels de l'image = largeur * hauteur 
 \param nbplan : 1 si image à niveaux de gris, 3 si image couleur
 */
void plusClair(unsigned char *img, int n, int nbplan)
{
    int i, v;
    for(i=0;i<n*nbplan;i=i+1) {
        v = 0.5 + img[i]*1.05;
        if (v>255) v = 255;
	img[i]=v;
    }
}



/*!
 \fn unsigned char * enNiveauxDeGris(unsigned char *img, int n, int nbplan)
 \param img : adresse du bitmap
 \param n : nombre de pixels de l'image = largeur * hauteur
 \param nbplan : 1 si image à niveaux de gris, 3 si image couleur. Doit valoir 3
 \brief délivre la transformée d'une image couleur en une image à niveaux de gris (luminance)
 \brief à l'aide de la formule appliquée aux pixels : 0.707*R+0.202*V+0.071*B
 
 */
unsigned char * enNiveauxDeGris(unsigned char *img, int n, int nbplan)
{
    unsigned char *res=NULL;
    int i;
    if (nbplan!=3) return NULL;
    res = (unsigned char *) malloc(n*sizeof(unsigned char));
    for(i=0;i<n;i=i+1)
        res[i] = 0.707*img[3*i]+0.202*img[3*i+1]+0.071*img[3*i+2];
    return res;
}

/*!
 \fn void deriv1x(unsigned char *img, int sx, int sy, int nbplan)
 \param img : adresse du bitmap
 \param sx : largeur de l'image
 \param sy : hauteur de l'image
 \param nbplan : 1 si image à niveaux de gris, 3 si image couleur
 \brief remplace chaque pixel à partir de la 2ème colonne par sa différence avec son voisin de gauche
 \brief les valeurs <= -128 sont ramenées à 0, celles >=127 à 255.
 \brief si image couleur traitement de chaque composante.
 
 */
void deriv1x(unsigned char *img, int sx, int sy, int nbplan)
{
    unsigned char *tmp=NULL;
    int i,j,k,res,adr=0;
    
    tmp=(unsigned char *)malloc(sx*sy*nbplan*sizeof(unsigned char));
    for(i=0;i<sy;i++)
        for(j=0;j<sx;j++) {
            if (j==0) {adr += nbplan; continue;}
            for(k=0;k<nbplan;k++) {
                res = 127 + (img[adr]-img[adr-nbplan])/2;
                if (res < 0) res = 0;
                if (res>255) res=255;
                tmp[adr] = res;
                adr = adr + 1;
            }
        }
    memcpy(img, tmp, sx*sy*nbplan*sizeof(unsigned char));
    free (tmp);
}

static int compval(unsigned char *a, unsigned char *b)
{
    return( (int)(*a - *b));
}


void meltimg(unsigned char *img, int sx, int sy, int np)
{
    int i,k,adr;
    int c,l;
    unsigned char *tmp;
    int N = sx*sy;
    
    tmp=calloc(sx*sy*np, sizeof(unsigned char));
    memcpy(tmp, img, sx*sy*np*sizeof(unsigned char));
    
    for(i=0;i<N;i++)
    {
        l = (int)(random()*sy/(float)RAND_MAX);
        c = (int)(random()*sx/(float)RAND_MAX);
        adr = l*sx*np + c*np;
        if (l<sy-1)
            if (tmp[adr]<tmp[adr+sx*np])
                for(k=0;k<np;k++) {
                    tmp[adr+sx*np+k] = tmp[adr+k];
                }
    }
    
    memcpy(img, tmp, sx*sy*np*sizeof(unsigned char));  
    free (tmp);
}


unsigned char filtrer(unsigned char *src, int i, int j, int sy, int sx, int ext, float *filtre)
{
    int k,l,I,J,adr=0;
    float somme=0.0;
    
    for(k=-ext; k<=ext; k++)
    {
        I=i+k;
        for(l=-ext; l<=ext; l++)
        {
            J=j+l;
            somme += (filtre[adr++] * (float)src[I*sx+J]);
        }
    }
    return( (unsigned char)(0.5 + somme) );
}


void convole(unsigned char *src, int sy, int sx, int ext, float *filtre, unsigned char *dest)
{
    int i,j;
    
    for(i=ext; i<sy-ext; i++)
    {
        for (j=ext; j<sx-ext; j++)
        {
            dest[i*sx+j]=filtrer(src, i, j, sy, sx, ext, filtre);
        }
    }
}


void appliquerFlou(unsigned char *src, int sy, int sx, int ext, unsigned char *dest)
{
    int i;
    float *filtre=malloc((2*ext+1)*(2*ext+1)*sizeof(float));
    for(i=0;i<(2*ext+1)*(2*ext+1);i++)filtre[i]=1.0/((2*ext+1)*(2*ext+1));
    convole(src, sy, sx, ext, filtre, dest);
}


unsigned char filtreMedian(unsigned char *src, int i, int j, int sy, int sx, int ext)
{
    int k,l,I,J,adr=0;
    unsigned char *liste;
    unsigned char res;
    
    liste = malloc((2*ext+1)*(2*ext+1));
    
    for(k=-ext; k<=ext; k++) {
        I=i+k;
        for(l=-ext; l<=ext; l++) {
            J=j+l;
            liste[adr++]=src[I*sx+J];
        }
    }
    qsort((char*)liste,(2*ext+1)*(2*ext+1),sizeof(unsigned char), (int(*)(const void*,const void*))compval);
    res=liste[((2*ext+1)*(2*ext+1))/2];
    free(liste);
    return(res);
}

void appliquerMedian(unsigned char *src, int sy, int sx, int ext, unsigned char *dest)
{
    int i,j;
    
    for(i=ext; i<sy-ext; i++)
    {
        for (j=ext; j<sx-ext; j++)
        {
            dest[i*sx+j]=filtreMedian(src, i, j, sy, sx, ext);
        }
    }
}




