

def monnaie(s) :
    if s>=50 :
        print("1 piece de 50 cents")
        s=s-50
            
    if s>=40 :
        print("2 pieces de 20 cents")
        s=s-40
    
    if s>=20 :
        print("1 piece de 20 cents")
        s=s-20
    
    if s>=10 :
        print("1 piece de 10 cents")
        s=s-10
    
    if s>=5 :
        print("1 piece de 5 cents")
        s=s-5
    
    if s>=4 :
        print("2 pieces de 2 cents")
        s=s-4
    
    if s>=2 :
        print("1 piece de 2 cents")
        s=s-2
    
    if s==1 :
        print("1 piece de 1 cents")
    


def diviseurViseur( a,  b) :
    return ((a%b)==0)


def plusGrandDesPlusPetits(r) :
    res=0
    p2=1
    while p2<=r :
        res=res+1
        p2=2*p2
    return res-1


monnaie(99)
print(diviseurViseur(12,3))
print(diviseurViseur(12,5))
print(plusGrandDesPlusPetits(100))
print(plusGrandDesPlusPetits(64))



