import random
import copy 
import os

class Cell:
    def __init__(self):
        self.alive = random.choice([False,True]);
        self.voisins = []

    def setVoisins(self,v):
        self.voisins = v;

    def revive(self):
        self.alive = True

    def kill(self):
        self.alive = False

    def isAlive(self):
        return self.alive

def alter(cells,taille):
    n = 0;
    alteredcells = copy.deepcopy(cells);
    for i in range(0,taille):
        for j in range(0,taille):
            #print("testing for ",i," ",j,": " ,end=" ");
            for k in [-1,1]:
                if(j+k < taille and j+k >= 0):  
                    #print("testing [",i,"][",j+k,"] <->", end=" ");
                    if(cells[i][j+k].isAlive()):
                        n = n + 1;
                        #print("is Alive A");
                    if(i-k < taille and i-k >= 0):
                        #print("testing [",i-k,"][",j+k,"] /", end=" ");
                        if(cells[i-k][j+k].isAlive()):
                            n = n + 1;
                            #print(" is alive B");
 
                if(i+k < taille and i+k >= 0):
                    #print("testing [",i+k,"][",j,"] up/down", end=" ");
                    if(cells[i+k][j].isAlive()):
                        n = n + 1;
                        #print(" is alive C");
                                       
                    if(j+k < taille and j+k >= 0):
                        #print("testing [",i+k,"][",j+k,"] \\", end=" ");
                        if(cells[i+k][j+k].isAlive()):
                            n = n + 1;
                            #print(" is alive D");
                  
            #print(" has ",n," neighbors ");
            if(n==3):
                alteredcells[i][j].revive();
            elif(n!=2 and cells[i][j].isAlive()):
                alteredcells[i][j].kill();
            n = 0;
     
    return alteredcells;


taille = int(input("taille du tableau: (<q pour quitter>"));

cells = [[Cell() for x in range(0,taille)] for y in range(0,taille)]

step = "cont";

OK = '\033[92m';
KO = '\033[91m';
END = '\033[0m';


for i in range(0,taille):
    for j in range(0,taille):
        if(cells[i][j].isAlive()):
            print(OK,'X',END,end= "")
        else:
            print(KO,'.',END,end= "")
    print("")

print("Enter \'q\' to quit");

while(step != "q"):
    cells = alter(cells,taille);
    for i in range(0,taille):
        for j in range(0,taille):
            if(cells[i][j].isAlive()):
                print(OK,'X',END,end= "")
            else:
                print(KO,'.',END,end= "")

        print("")
    step = input()
    os.system("clear");

#addition

A = [[random.randint(0,9) for x in range(0,3)] for y in range(0,3)]
B = [[random.randint(0,9) for x in range(0,3)] for y in range(0,3)]

print("A:");
for i in range(0,3):
        for j in range(0,3):
            print(A[i][j],end=" ");
        print("");    

print("B:");
for i in range(0,3):
        for j in range(0,3):
            print(B[i][j],end=" ");
        print("");

print("A+B");
for i in range(0,3):
        for j in range(0,3):
            print(A[i][j] + B[i][j],end=" ");
        print("");

#multiplication

C = [[0 for x in range(0,3)] for y in range(0,3)]


print("A*B:")
for i in range(0,3):
        for j in range(0,3):
            C[i][j] = (A[i][j] * B[j][i]) + C[i][j];

for i in range(0,3):
        for j in range(0,3):
            print(C[i][j], end=" ");
        print("");


